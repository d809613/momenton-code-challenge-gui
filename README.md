# GUI for Momenton Code Challenge Solution

This GUI was built by using Angular 5.2.0. It consists of 4 parts:

* Core - includes home, header, footer and 404 components.

* Layouts - includes hierarchical table, tree, treemap and pack layout components. Note cluster and force directed graph can be also found in source code but they are not completed.

* Data Center - includes data management, data source table components and data service. Note all http requests are send out inside data service.

* Models - includes a Employee model.

The app is currently hosted in a private AWS S3 bucket for [demo](http://cloud.jing-momentoncodechallenge.com.s3-website-ap-southeast-2.amazonaws.com/) purpose.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* Angular CLI 1.7.4
* NPM 5.6.0
* Node 6.11.0

### Installing

Clone repo to your local system:

```
git clone https://d809613@bitbucket.org/d809613/momenton-code-challenge-gui.git
```

cd to the project folder:

```
cd momenton-code-challenge-gui
```

build dependencies:

```
npm install
```

host gui app locally:

```
ng serve
```

change base http url to your REST localhost in DataService.DataService

## Deployment

This GUI app has been depoyed to a private AWS S3 bucket for demo purpose.

## Built With

* [d3.js](https://d3js.org/) - Data-Driven Documents
* [boostrap](https://getbootstrap.com/) - CSS library
* [angular](https://angular.io/) - JavaScript Framwork

## Authors

* **Jing Zhou**

