/**
 * App Data Management Component
 *
 * Copyright (c) 2018 All Rights Reserved.
 * @author Jing Zhou
 * @since 1.0.0
 */
import {Component, OnInit, ViewChild} from '@angular/core';
import {DataService} from '../data.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-data-management',
  templateUrl: './data-management.component.html',
  styleUrls: ['./data-management.component.css']
})
export class DataManagementComponent implements OnInit {
  // declare and bind ng template form object
  @ViewChild('f') employee: NgForm;
  data = [];
  // types array for the left panel list
  types = [
    {key: 'tableLayout', value: 'Hierarchical Table', desc: 'Plan HTML', icon: 'fab fa-html5'},
    {key: 'treeLayout', value: 'Tree Layout', desc: 'D3.js', icon: 'fab fa-js-square'},
    {key: 'treemapLayout', value: 'Treemap Layout', desc: 'D3.js', icon: 'fab fa-js-square'},
    {key: 'packLayout', value: 'Pack Layout', desc: 'D3.js', icon: 'fab fa-js-square'},
  ];
  // indicates which panel is selected and table layout is the default value
  activeLayout: string = 'tableLayout';

  idNotUnique = false;

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.dataService.fetchRawData().subscribe(
      response => {
        this.data = response[this.activeLayout];
      }
    );
    // reset the form
    this.employee.resetForm();
  }

  /**
   * @function: setlayoutType
   *
   * @param: layout: string
   *
   * @Complexity: O(1)
   *
   * @description: callback function to panel click event which sets the active
   *               layout to the layout of clicked panel
   *
   */
  setlayoutType(layout: string): void {
    this.activeLayout = layout;
  }

  /**
   * @function: onFormSubmit
   *
   * @param: None
   *
   * @Complexity: O(1)
   *
   * @description: callback function to form submit button
   *               it will add new employee to the appointed data source
   *
   */
  onFormSubmit(): void {
    // construct Employee obj from form obj
    const employee = {
      name: this.employee.value.name,
      id: this.employee.value.id,
      manager: this.employee.value.manager
    };
    // check if ID is unique
    if (!this.checkIfIdUnique(employee.id)) {
      this.idNotUnique = true;
    } else {
      // call addEmployee API to add employee
      this.dataService.addEmployee(employee, this.activeLayout).subscribe(
        response => {
          this.dataService.dataChanged.next(true);
        }
      );
    }
  }

  /**
   * @function: checkIfIdUnique
   *
   * @param: id: number, layout: string
   *
   * @Complexity: O(1)
   *
   * @description: an API method to check ID of given employee has been used
   *
   */
  checkIfIdUnique(id: number): boolean {
    const employees = this.data.filter(e => {
      return e.id === Number(id);
    });

    return employees.length === 0;
  }

  /**
   * @function: delete
   *
   * @param: None
   *
   * @Complexity: O(1)
   *
   * @description: callback function to delete button
   *               it will delete the last employee from
   *               the appointed data source
   *
   */
  delete(): void {
    this.dataService.deleteEmployee(this.activeLayout).subscribe(
      response => {
        this.dataService.dataChanged.next(true);
      }
    );
  }

  idInputFocus(): void {
    this.idNotUnique = false;
  }
}
