/**
 * Data Service Class
 *
 * Copyright (c) 2018 All Rights Reserved.
 * @author Jing Zhou
 * @since 1.0.0
 */
import {Employee} from '../models/Employee';
import {Subject} from 'rxjs/Subject';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

const base = 'https://desolate-depths-66318.herokuapp.com';

@Injectable()
export class DataService {
  // a map that stores key: Manager to value: Employee[]
  managers = {};

  // a number var that stores the max rank.
  // initially 0, will be constructed in function recuriveRankAssignment
  maxRank = 0;

  // an array that stores the root manager (CEO)
  topManagers = [];

  // a map that stores invalid employees:
  // 1. employees with ghost manager (integrity constraint)
  // 2. employees with looped hierarchy (infinite loop)
  // it will first contains all employees during managers array construction
  // then gets cleared out during rankedEmployees array construction
  invalidEmployees = {};

  // an array that stores the resulting output for table layout:
  // array index: represents values on y axis (aka level)
  // rank attribute in array element: represents values on x axis (aka rank)
  rankedEmployees = [];

  // a subject as event publisher, it fires event when data is updated
  // so that all its subscribers can catch this event and update their views accordingly
  dataChanged: Subject<boolean> = new Subject<boolean>();

  constructor(private http: HttpClient) {
  }

  /**
   * @function: initRawData
   *
   * @param: None
   *
   * @Return: Observable<any> - http response
   *
   * @Complexity: O(1)
   *
   * @description: send http request and return its response as an observable
   *
   */
  fetchRawData(): Observable<any> {
    return this.http.get(base + '/codechallenge/data');
  }

  /**
   * @function: constructInvalidEmployees
   *
   * @param: employee: Employee
   *
   * @Complexity: O(1)
   *
   * @description: add param employee into invalidEmployees array
   *
   */
  constructInvalidEmployees(employee: Employee): void {
    this.invalidEmployees[employee.id] = employee;
  }

  /**
   * @function: constructManagers
   *
   * @param: employee: Employee
   *
   * @Complexity: O(1)
   *
   * @description: constructing managers array
   *
   */
  constructManagers(employee: Employee): void {
    // if the manager of input employee is already in managers array
    if (employee.manager in this.managers) {

      // add employee to its children array
      this.managers[employee.manager].push(employee);

      // if the manager of input employee doesn't exist in managers array
    } else {

      // add a new entry for the manager
      this.managers[employee.manager] = [employee];
    }
  }

  /**
   * @function: constructTopManagers
   *
   * @param: employee: Employee
   *
   * @Complexity: O(1)
   *
   * @description: add manager to topManager array
   *
   */
  constructTopManagers(employee: Employee): void {
    this.topManagers.push(employee);
  }

  /**
   * @function: recuriveRankAssignment
   *
   * @param: employee: Employee, rank: number
   *
   * @Complexity: O(N)
   *
   * @description: implements a recursive algorithm that resolves
   *               the code challenge problem.
   *               the function takes root manager initially and
   *               traverse all the way down to a leaf employee.
   *               it will eventually visit all valid employees and
   *               construct rankedEmployees array
   *
   */
  recuriveRankAssignment(employee: Employee, rank: number): void {
    // declare a children array for the iput employee
    let managedEmployees;

    // assign rank
    employee.rank = rank;

    // update max rank
    if (rank > this.maxRank) {
      this.maxRank = rank;
    }

    // delete visited employee out from invalidEmployees
    delete this.invalidEmployees[employee.id];

    // push visited employee into rankedEmployees in a sequential order as the call stack
    // so that the y-axis value (or level) can be set correctly
    this.rankedEmployees.push(employee);

    // fetch input employee's children
    managedEmployees = this.managers[employee.id];

    // recursively call the same funtion to process all the children elements
    if (managedEmployees) {
      managedEmployees.forEach(subordinate => this.recuriveRankAssignment(subordinate, rank + 1));
    }

  }

  /**
   * @function: constructRankedEmployees
   *
   * @param: data: any
   *
   * @Complexity: O(N)
   *
   * @description: main function to construct RankedEmployees array
   *
   */
  constructRankedEmployees(data: any): void {
    // fetch the right data source with given layout
    data.forEach(row => {
      const employee = new Employee(row.name, row.id, row.manager, 0);

      // add all visited employees to the invalid employees array
      this.constructInvalidEmployees(employee);

      // if employee has a manager
      if (employee.manager) {

        // add it into managers map
        this.constructManagers(employee);

        // if the employee doesn't have a manager
      } else {

        // add it into topManagers array
        this.constructTopManagers(employee);
      }
    });

    // call recuriveRankAssignment function to constructed rankedEmployees array
    this.topManagers.forEach(manager => this.recuriveRankAssignment(manager, 1));
  }

  /**
   * @function: constructTreeData
   *
   * @param: data: any[]
   *
   * @Complexity: O(N^2)
   *
   * @description: a non-recursive implementation that solves the same problem in the code challenge but construct
   *               a tree data structure instead.
   *               recursive function is no doubt the primary choice but I WANT TO DEMO THAT ALL RECURSIVE
   *               ALGORITHMS CAN BE REWRITTEN IN A NON-RECURSIVE WAY WITH STACK
   *
   */
  constructTreeData(data: any): void {
    const keys = [],
      inputData = data;
    inputData.map(row => {
      row.children = [];
      keys.push(row.id);
    });
    const roots = inputData.filter(row => {
      return keys.indexOf(row.manager) === -1;
    });
    // a stack var, the key var in this non-recursive algorithm
    const nodes = [];
    roots.map(e => {
      nodes.push(e);
    });
    while (nodes.length > 0) {
      const node = nodes.pop();
      const children = inputData.filter(row => {
        return row.manager === node.id;
      });
      if (children.length === 0) {
        node.value = 1;
      }
      children.map(c => {
        node.children.push(c);
        nodes.push(c);
      });
    }

    // return the first root only
    return roots[0];
  }

  /**
   * @function: getProcesedData
   *
   * @param: layout: string, data: any
   *
   * @return: maxRank: String[] - build an dummy array from this.maxRank with length == this.maxRank
   *          rankedEmployees: Employee[]
   *          invalidEmployees: {}
   *
   * @Complexity: O(N)
   *
   * @description: resets all vars and reconstruct rankedEmployees, maxRank array and invalidEmployees array
   *
   */
  getProcesedData(data: any): { maxRank: String[], rankedEmployees: Employee[], invalidEmployees: {} } {
    this.managers = {};
    this.maxRank = 0;
    this.topManagers = [];
    this.rankedEmployees = [];
    this.invalidEmployees = {};
    this.constructRankedEmployees(data);
    return {
      maxRank: Array(this.maxRank).fill(''), // dummy an dummy array from this.maxRank with length == this.maxRank
      rankedEmployees: this.rankedEmployees,
      invalidEmployees: this.invalidEmployees
    };
  }

  /**
   * @function: getProcessedTreeData
   *
   * @param: layout: string, data: any
   *
   * @return: any - a tree data set
   *
   * @Complexity: O(N^2)
   *
   * @description: main function that calls constructTreeData to construct tree data set
   *
   */
  getProcessedTreeData(data: any): any {
    return this.constructTreeData(data);
  }

  /**
   * @function: addEmployee
   *
   * @param: employee: Employee, layout: string
   *
   * @Complexity: O(1)
   *
   * @description: an API method to add employee to appointed data source
   *
   */
  addEmployee(employee: Employee, layout: string): Observable<any> {
    const e = {
        id: employee.id,
        name: employee.name,
        manager: employee.manager
      },
      url = base + `/codechallenge/layout-data/add/${layout}`;
    return this.http.post(url, e);
  }

  /**
   * @function: deleteEmployee
   *
   * @param: layout: string
   *
   * @Complexity: O(1)
   *
   * @description: an API method to delete the last employee from appointed data source
   *
   */
  deleteEmployee(layout: string): Observable<any> {
    const  url = base + '/codechallenge/layout-data/delete';
    return this.http.post(url, layout);
  }

}
