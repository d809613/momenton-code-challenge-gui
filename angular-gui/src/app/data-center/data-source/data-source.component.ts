/**
 * App Data Source Table Component
 *
 * Copyright (c) 2018 All Rights Reserved.
 * @author Jing Zhou
 * @since 1.0.0
 */

import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {DataService} from '../data.service';
import {Subscription} from 'rxjs/Subscription';

// a simple interface for row element
export interface Element {
  id: number;
  name: string;
  manager: number;
}

@Component({
  selector: 'app-data-source',
  templateUrl: './data-source.component.html',
  styleUrls: ['./data-source.component.css']
})
export class DataSourceComponent implements OnInit, OnChanges, OnDestroy {
  // reads layoutType from parent to determine the date source
  @Input('layoutType') layoutType: string;
  // headers array
  displayedColumns = ['Id', 'Name', 'Manager'];
  dataSource: Element[] = [];
  // suSubscription to data change event
  dataChangedSub: Subscription;

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    // init data source
    this.initDataSource();
    // re-init date source when receive data change event from data center
    this.dataChangedSub = this.dataService.dataChanged.subscribe(
      event => {
        this.initDataSource();
      }
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    this.initDataSource();
  }

  ngOnDestroy() {
    this.dataChangedSub.unsubscribe();
  }

  /**
   * @function: initDataSource
   *
   * @param: None
   *
   * @Complexity: O(N)
   *
   * @description: read raw data from data service and construct data source for the table view
   *
   */
  initDataSource(): void {
    this.dataService.fetchRawData().subscribe(
      response => {
        this.dataSource = [];
        response[this.layoutType].forEach((row: any) => {
          this.dataSource.push({
            id: row.id,
            name: row.name,
            manager: row.manager
          });
        });
      }
    );

  }
}
