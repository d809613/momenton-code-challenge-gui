import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppComponent} from './app.component';
import {HeaderComponent} from './core/header/header.component';
import {HierarchicalTableComponent} from './layouts/hierarchical-table/hierarchical-table.component';
import {ForceDirectedGraphComponent} from './layouts/force-directed-graph/force-directed-graph.component';
import {D3Service} from 'd3-ng2-service';
import {TreeLayoutComponent} from './layouts/tree-layout/tree-layout.component';
import {ClusterLayoutComponent} from './layouts/cluster-layout/cluster-layout.component';
import {TreemapLayoutComponent} from './layouts/treemap-layout/treemap-layout.component';
import {PackLayoutComponent} from './layouts/pack-layout/pack-layout.component';
import {FooterComponent} from './core/footer/footer.component';
import {HomeComponent} from './core/home/home.component';
import {PageNotFoundComponent} from './core/page-not-found/page-not-found.component';
import {AppRoutingModule} from './app-routing.module';
import {DataService} from './data-center/data.service';
import { DataSourceComponent } from './data-center/data-source/data-source.component';
import { DataManagementComponent } from './data-center/data-management/data-management.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HierarchicalTableComponent,
    ForceDirectedGraphComponent,
    TreeLayoutComponent,
    ClusterLayoutComponent,
    TreemapLayoutComponent,
    PackLayoutComponent,
    FooterComponent,
    HomeComponent,
    PageNotFoundComponent,
    DataSourceComponent,
    DataManagementComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [D3Service, DataService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
