/**
 * App Hierarchical Table Component
 *
 * Copyright (c) 2018 All Rights Reserved.
 * @author Jing Zhou
 * @since 1.0.0
 */

import {Component, OnInit} from '@angular/core';
import {DataService} from '../../data-center/data.service';
import {Employee} from '../../models/Employee';

@Component({
  selector: 'app-hierarchical-table',
  templateUrl: 'hierarchical-table.html',
  styleUrls: ['hierarchical-table.css']
})

export class HierarchicalTableComponent implements OnInit {
  dataSource: { maxRank: String[], rankedEmployees: Employee[], invalidEmployees: {} };
  // flag used to determine which component to render: ;ayout or source
  displaySource = false;

  buttonText = 'Show Source';

  constructor(private dataService: DataService) {
  };

  ngOnInit(): void {
    // fetch data source from data service
    this.dataService.fetchRawData().subscribe(
      response => {
        this.dataSource = this.dataService.getProcesedData(response['tableLayout']);
        console.log(this.dataSource);
      }
    );
  }

  /**
   * @function: showSource
   *
   * @param: None
   *
   * @Complexity: O(1)
   *
   * @description: callback function for the button click evebt.
   *               it flags displaySource var and switches buttonText
   *
   */
  showSource(): void {
    this.displaySource = !this.displaySource;
    !this.displaySource ? this.buttonText = 'Show Source' : this.buttonText = 'Show Result';
  }
}
