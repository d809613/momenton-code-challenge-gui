/**
 * NOTE THIS COMPOENET IS NOT USED IN THIS PRPJECT!!!
 *
 * Copyright (c) 2018 All Rights Reserved.
 * @author Jing Zhou
 * @since 1.0.0
 */
import {Component, OnInit, ElementRef, ViewEncapsulation} from '@angular/core';
import {D3Service, D3} from 'd3-ng2-service';

@Component({
  selector: 'app-cluster-layout',
  templateUrl: './cluster-layout.component.html',
  styleUrls: ['./cluster-layout.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ClusterLayoutComponent implements OnInit {

  private d3: D3;
  private parentNativeElement: any;

  data = {
    'name': 'A1',
    'children': [
      {
        'name': 'B1',
        'children': [
          {
            'name': 'C1',
            'value': 100
          },
          {
            'name': 'C2',
            'value': 300
          },
          {
            'name': 'C3',
            'value': 200
          }
        ]
      },
      {
        'name': 'B2',
        'value': 200
      }
    ]
  };

  constructor(element: ElementRef, d3Service: D3Service) {
    this.d3 = d3Service.getD3();
    this.parentNativeElement = element.nativeElement;
  }

  ngOnInit() {
    const d3 = this.d3;

    if (this.parentNativeElement !== null) {

      const clusterLayout = d3.cluster()
          .size([700, 500]),
        root = d3.hierarchy(this.data);

      clusterLayout(root);

      d3.select(this.parentNativeElement).select('svg g.nodes')
        .selectAll('circle.node')
        .data(root.descendants())
        .enter()
        .append('circle')
        .classed('node', true)
        .attr('cx', function (d) {
          return d['x'];
        })
        .attr('cy', function (d) {
          return d['y'];
        })
        .attr('r', 4);

      d3.select(this.parentNativeElement).select('svg g.links')
        .selectAll('line.link')
        .data(root.links())
        .enter()
        .append('line')
        .classed('link', true)
        .attr('x1', function (d) {
          return d.source['x'];
        })
        .attr('y1', function (d) {
          return d.source['y'];
        })
        .attr('x2', function (d) {
          return d.target['x'];
        })
        .attr('y2', function (d) {
          return d.target['y'];
        });


    }
  }

}
