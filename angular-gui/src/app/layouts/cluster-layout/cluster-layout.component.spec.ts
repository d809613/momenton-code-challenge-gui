import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClusterLayoutComponent } from './cluster-layout.component';

describe('ClusterLayoutComponent', () => {
  let component: ClusterLayoutComponent;
  let fixture: ComponentFixture<ClusterLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClusterLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClusterLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
