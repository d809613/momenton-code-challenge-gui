/**
 * App Pack Layout Component
 *
 * Copyright (c) 2018 All Rights Reserved.
 * @author Jing Zhou
 * @since 1.0.0
 */


import {Component, OnInit, ElementRef, ViewEncapsulation} from '@angular/core';
import {D3Service, D3} from 'd3-ng2-service';
import {DataService} from '../../data-center/data.service';

@Component({
  selector: 'app-pack-layout',
  templateUrl: './pack-layout.component.html',
  styleUrls: ['./pack-layout.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PackLayoutComponent implements OnInit {

  private d3: D3;
  private parentNativeElement: any;
  data: {};
  displaySource = false;
  buttonText = 'Show Source';

  constructor(private element: ElementRef, private d3Service: D3Service, private dataService: DataService) {
  }

  /**
   * @function: ngOnInit
   *
   * @param: None
   *
   * @Complexity: O(N)
   *
   * @description: modify the DOM and render pack layout
   *               pretty standard d3 code.
   *
   */
  ngOnInit() {
    this.dataService.fetchRawData().subscribe(
      response => {
        this.d3 = this.d3Service.getD3();
        this.parentNativeElement = this.element.nativeElement;
        this.data = this.dataService.getProcessedTreeData(response['packLayout']);
        const d3 = this.d3;

        if (this.parentNativeElement !== null) {

          const packLayout = d3.pack()
            .size([720, 450]).padding(32);

          const rootNode = d3.hierarchy(this.data);

          rootNode.sum(function (d) {
            return d['value'];
          });

          packLayout(rootNode);

          const nodes = d3.select('.pack-layout svg g')
            .selectAll('g')
            .data(rootNode.descendants())
            .enter()
            .append('g')
            .attr('transform', function (d) {
              return 'translate(' + [d['x'], d['y']] + ')';
            });

          nodes
            .append('circle')
            .attr('r', function (d) {
              return d['r'];
            });

          nodes
            .append('text')
            .attr('dx', -16)
            .attr('dy', 4)
            .text(function (d) {
              return d.children === undefined ? d.data['name'] : '';
            });


        }
      }
    );
  }

  /**
   * @function: showSource
   *
   * @param: None
   *
   * @Complexity: O(1)
   *
   * @description: callback function for the button click evebt.
   *               it flags displaySource var and switches buttonText
   *
   */
  showSource(): void {
    this.displaySource = !this.displaySource;
    !this.displaySource ? this.buttonText = 'Show Source' : this.buttonText = 'Show Result';
  }

}
