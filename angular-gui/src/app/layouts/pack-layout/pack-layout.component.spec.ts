import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PackLayoutComponent } from './pack-layout.component';

describe('PackLayoutComponent', () => {
  let component: PackLayoutComponent;
  let fixture: ComponentFixture<PackLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PackLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
