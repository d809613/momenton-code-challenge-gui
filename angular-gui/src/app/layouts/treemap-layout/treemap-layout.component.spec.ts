import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreemapLayoutComponent } from './treemap-layout.component';

describe('TreemapLayoutComponent', () => {
  let component: TreemapLayoutComponent;
  let fixture: ComponentFixture<TreemapLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreemapLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreemapLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
