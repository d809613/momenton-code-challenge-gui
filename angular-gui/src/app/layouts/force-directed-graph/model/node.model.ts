/**
 * NOTE THIS COMPOENET IS NOT USED IN THIS PRPJECT!!!
 *
 * Copyright (c) 2018 All Rights Reserved.
 * @author Jing Zhou
 * @since 1.0.0
 */

import {SimulationNodeDatum} from 'd3-force';

export class Node implements SimulationNodeDatum {
  index?: number;
  x?: number;
  y?: number;
  vx?: number;
  vy?: number;
  fx?: number | null;
  fy?: number | null;

  id: string | number;
  linkCount: number = 0;
  sev: string;

  constructor(id: string | number) {
    this.id = id;
  }

  get r(): number {
    return Number('70');
  }
}
