/**
 * NOTE THIS COMPOENET IS NOT USED IN THIS PRPJECT!!!
 *
 * Copyright (c) 2018 All Rights Reserved.
 * @author Jing Zhou
 * @since 1.0.0
 */

import {SimulationLinkDatum} from 'd3-force';
import { Node } from './node.model';

export class Link implements SimulationLinkDatum<Node> {
  // Optional - defining optional implementation properties - required for relevant typing assistance
  index?: number;

  // Must - defining enforced implementation properties
  source: Node | string | number;
  target: Node | string | number;

  constructor(source, target) {
    this.source = source;
    this.target = target;
  }
}
