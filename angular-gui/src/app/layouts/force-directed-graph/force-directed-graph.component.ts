/**
 * NOTE THIS COMPOENET IS NOT USED IN THIS PRPJECT!!!
 *
 * Copyright (c) 2018 All Rights Reserved.
 * @author Jing Zhou
 * @since 1.0.0
 */

import {Component, ElementRef, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {
  D3Service,
  D3,
  Axis,
  ScaleLinear,
  ScaleOrdinal,
  Selection,
  Simulation,
  Transition
} from 'd3-ng2-service';
import {ForceLink} from 'd3-force';
import {Link} from './model/link.model';
import {Node} from './model/node.model';

@Component({
  selector: 'app-force-directed-graph',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './force-directed-graph.component.html',
  styleUrls: ['./force-directed-graph.component.css']
})
export class ForceDirectedGraphComponent implements OnInit {
  nodes: Node[] = [];
  links: Link[] = [];

  private d3: D3;
  private parentNativeElement: any;
  private d3Svg: Selection<SVGSVGElement, any, null, undefined>;


  constructor(element: ElementRef, d3Service: D3Service) {
    this.d3 = d3Service.getD3();
    this.parentNativeElement = element.nativeElement;
  }


  ngOnInit() {
    let d3 = this.d3;
    let width: number;
    let height: number;
    let d3ParentElement: Selection<HTMLElement, any, null, undefined>;
    let d3Svg: Selection<SVGSVGElement, any, null, undefined>;
    this.initData();

    if (this.parentNativeElement !== null) {

      d3ParentElement = d3.select(this.parentNativeElement);
      d3Svg = this.d3Svg = d3ParentElement.select<SVGSVGElement>('svg');

      width = +d3Svg.attr('width');
      height = +d3Svg.attr('height');

      var color = d3.scaleOrdinal(d3.schemeCategory10);

      var simulation = d3.forceSimulation()
        .force('link', d3.forceLink().id(function (d) {
          return d['id'];
        }))
        .force('charge', d3.forceManyBody())
        .force('center', d3.forceCenter(width / 2, height / 2));

      var link = d3Svg.append<SVGGElement>('g')
        .attr('class', 'links')
        .selectAll('line')
        .data(this.links)
        .enter().append('line');

      var node = d3Svg.append<SVGGElement>('g')
        .attr('class', 'nodes')
        .selectAll('circle')
        .data(this.nodes)
        .enter().append('circle')
        .attr('r', 5);


      node.append('title')
        .text(function (d) {
          return d.id;
        });

      simulation
        .nodes(this.nodes)
        .on('tick', ticked);

      simulation.force<ForceLink<any, any>>('link').links(this.links);


    }

    function ticked() {
      link
        .attr('x1', function (d: any) {
          return d.source;
        })
        .attr('y1', function (d: any) {
          return d.source;
        })
        .attr('x2', function (d: any) {
          return d.target;
        })
        .attr('y2', function (d: any) {
          return d.target;
        });

      node
        .attr('cx', function (d: any) {
          return d.id;
        })
        .attr('cy', function (d: any) {
          return d.id;
        });
    }

    function dragstarted(d) {
      if (!d3.event.active) simulation.alphaTarget(0.3).restart();
      d.fx = d.x;
      d.fy = d.y;
    }

    function dragged(d) {
      d.fx = d3.event.x;
      d.fy = d3.event.y;
    }

    function dragended(d) {
      if (!d3.event.active) simulation.alphaTarget(0);
      d.fx = null;
      d.fy = null;
    }
  }

  initData(): void {
    let node_0: Node = new Node(0),
      node_1: Node = new Node(1),
      node_2: Node = new Node(2),
      node_3: Node = new Node(3),
      node_4: Node = new Node(4);

    this.nodes.push(node_0);
    this.nodes.push(node_1);
    this.nodes.push(node_2);
    this.nodes.push(node_3);
    this.nodes.push(node_4);
    //links
    this.links.push(new Link(0, 1));
    this.links.push(new Link(1, 0));

    this.links.push(new Link(0, 2));
    this.links.push(new Link(2, 0));

    this.links.push(new Link(3, 0));
    this.links.push(new Link(3, 1));
    this.links.push(new Link(3, 2));

    this.links.push(new Link(0, 3));
    this.links.push(new Link(1, 3));
    this.links.push(new Link(2, 3));

    this.links.push(new Link(4, 3));
    this.links.push(new Link(3, 4));
  }

}
