/**
 * App Tree Layout Component
 *
 * Copyright (c) 2018 All Rights Reserved.
 * @author Jing Zhou
 * @since 1.0.0
 */


import {Component, OnInit, ElementRef, ViewEncapsulation} from '@angular/core';
import {D3Service, D3} from 'd3-ng2-service';
import {DataService} from '../../data-center/data.service';

@Component({
  selector: 'app-tree-layout',
  templateUrl: './tree-layout.component.html',
  styleUrls: ['./tree-layout.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TreeLayoutComponent implements OnInit {

  private d3: D3;
  private parentNativeElement: any;
  data: {};
  displaySource = false;
  buttonText = 'Show Source';

  constructor(private element: ElementRef, private d3Service: D3Service, private dataService: DataService) {
  }

  /**
   * @function: ngOnInit
   *
   * @param: None
   *
   * @Complexity: O(N)
   *
   * @description: modify the DOM and render tree layout
   *               pretty standard d3 code.
   *
   */
  ngOnInit() {
    this.dataService.fetchRawData().subscribe(
      response => {
        this.d3 = this.d3Service.getD3();
        this.parentNativeElement = this.element.nativeElement;
        this.data = this.dataService.getProcessedTreeData(response['treeLayout']);
        const d3 = this.d3,
          root = d3.hierarchy(this.data),
          treeLayout = d3.tree();

        treeLayout.size([700, 500]);
        treeLayout(root);

        if (this.parentNativeElement !== null) {
          // set the dimensions and margins of the diagram
          const margin = {top: 20, right: 90, bottom: 30, left: 90},
            width = 660 - margin.left - margin.right,
            height = 500 - margin.top - margin.bottom;

          // declares a tree layout and assigns the size
          const treemap = d3.tree()
            .size([height, width]);

          //  assigns the data to a hierarchy using parent-child relationships
          let nodes = d3.hierarchy(this.data, function (d: any) {
            return d.children;
          });

          // maps the node data to the tree layout
          nodes = treemap(nodes);

          // append the svg object to the body of the page
          // appends a 'group' element to 'svg'
          // moves the 'group' element to the top left margin
          const svg = d3.select('.tree-layout').append('svg')
              .attr('width', width + margin.left + margin.right)
              .attr('height', height + margin.top + margin.bottom),
            g = svg.append('g')
              .attr('transform',
                'translate(' + margin.left + ',' + margin.top + ')');

          // adds the links between the nodes
          const link = g.selectAll('.link')
            .data(nodes.descendants().slice(1))
            .enter().append('path')
            .attr('class', 'link')
            .attr('d', function (d) {
              return 'M' + d['y'] + ',' + d['x']
                + 'C' + (d['y'] + d.parent['y']) / 2 + ',' + d['x']
                + ' ' + (d['y'] + d.parent['y']) / 2 + ',' + d.parent['x']
                + ' ' + d.parent['y'] + ',' + d.parent['x'];
            });

          // adds each node as a group
          const node = g.selectAll('.node')
            .data(nodes.descendants())
            .enter().append('g')
            .attr('class', function (d) {
              return 'node' +
                (d.children ? ' node--internal' : ' node--leaf');
            })
            .attr('transform', function (d) {
              return 'translate(' + d['y'] + ',' + d['x'] + ')';
            });

          // adds the circle to the node
          node.append('circle')
            .attr('r', 22);

          // adds the text to the node
          node.append('text')
            .attr('dy', '-1.5em')
            .attr('x', function (d) {
              return d.children ? -13 : 13;
            })
            .style('text-anchor', function (d) {
              return d.children ? 'end' : 'start';
            })
            .text(function (d) {
              return d.data.name;
            });

        }
      }
    );

  }

  /**
   * @function: showSource
   *
   * @param: None
   *
   * @Complexity: O(1)
   *
   * @description: callback function for the button click evebt.
   *               it flags displaySource var and switches buttonText
   *
   */
  showSource(): void {
    this.displaySource = !this.displaySource;
    !this.displaySource ? this.buttonText = 'Show Source' : this.buttonText = 'Show Result';
  }

}
