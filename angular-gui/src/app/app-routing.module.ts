import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './core/home/home.component';
import {HierarchicalTableComponent} from './layouts/hierarchical-table/hierarchical-table.component';
import {ForceDirectedGraphComponent} from './layouts/force-directed-graph/force-directed-graph.component';
import {ClusterLayoutComponent} from './layouts/cluster-layout/cluster-layout.component';
import {PackLayoutComponent} from './layouts/pack-layout/pack-layout.component';
import {TreeLayoutComponent} from './layouts/tree-layout/tree-layout.component';
import {TreemapLayoutComponent} from './layouts/treemap-layout/treemap-layout.component';
import {PageNotFoundComponent} from './core/page-not-found/page-not-found.component';
import {DataManagementComponent} from './data-center/data-management/data-management.component';

const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'hierarchical-table', component: HierarchicalTableComponent},
  {path: 'force-directed-graph', component: ForceDirectedGraphComponent},
  {path: 'cluster-layout', component: ClusterLayoutComponent},
  {path: 'pack-layout', component: PackLayoutComponent},
  {path: 'tree-layout', component: TreeLayoutComponent},
  {path: 'treemap-layout', component: TreemapLayoutComponent},
  {path: 'data-management', component: DataManagementComponent},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
