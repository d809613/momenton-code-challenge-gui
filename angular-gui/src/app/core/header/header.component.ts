/**
 * App Header Component
 *
 * Copyright (c) 2018 All Rights Reserved.
 * @author Jing Zhou
 * @since 1.0.0
 */

import {Component, OnInit, HostListener, ViewChild, ElementRef} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: 'header.html',
  styleUrls: ['header.css']
})

export class HeaderComponent implements OnInit {
  @ViewChild('navRef') navRef: ElementRef;
  @HostListener('window:scroll', [])
  /**
   * @function: onWindowScroll
   *
   * @param: none
   *
   * @Complexity: O(1)
   *
   * @description: detects onScroll event and change header (background) style when scroll over 100px.
   *
   */
  onWindowScroll() {
    const number = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    if (number > 100) {
      this.navRef.nativeElement.style.backgroundColor = '#f6f6f6';
    } else {
      this.navRef.nativeElement.style.backgroundColor = 'white';
    }
  }
  constructor() {
  }

  ngOnInit(): void {
  }
}
