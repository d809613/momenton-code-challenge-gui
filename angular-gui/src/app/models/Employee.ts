export class Employee {
  constructor(public name: string,
              public id: string | number,
              public manager: string | number,
              public rank?: number) {
    this.name = name;
    this.id = id;
    this.manager = manager;
    this.rank = rank;
  }
}
